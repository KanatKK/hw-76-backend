const router = require("express").Router();
const db = require("../messagesDB");

router.get("/", (req, res) => {
    const messages = db.getMessages();
    const dateTimeMessages = [];
    if (req.query.datetime !== undefined) {
        if (isNaN(new Date(req.query.datetime.toString())) === true) {
            res.send({error: 'Invalid date'});
        } else {
            for (let i = 0; i <= messages.length - 1; i++) {
                if (messages[i].datetime === req.query.datetime) {
                    for (let k = i + 1; k <= messages.length - 1; k++) {
                        dateTimeMessages.push(messages[k]);
                    }
                }
            }
            res.send(dateTimeMessages);
        }
    } else {
        res.send(messages);
    }
});
router.post("/", (req, res) => {
    db.addMessage(req.body);
    const messages = db.getMessages();
    res.send(messages);
});

module.exports = router;